package br.com.metasix.fragment;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.Arrays;
import java.util.List;

import br.com.metasix.agenda.R;
import br.com.metasix.agenda.modelo.Prova;
import br.com.metasix.ui.activity.ProvasActivity;

public class ListaProvasFragment extends Fragment{

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_lista_provas, container, false);


        List<String> topicosPort = Arrays.asList("Sujeito", "Objeto direto", "Objeto indireto");
        Prova provaPortugues = new Prova("portugues", "25/05/2016", topicosPort);

        List<String> topicoMat = Arrays.asList("Equaçoes de segundo grau", "Trigonometria");
        Prova provaMat = new Prova("matematica", "27/05/2016", topicoMat);


        List<Prova> provas = Arrays.asList(provaPortugues, provaMat);

        ArrayAdapter<Prova> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, provas);

        ListView lista  = (ListView) view.findViewById(R.id.provas_lista);
        lista.setAdapter(adapter);


        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Prova prova  = (Prova) parent.getItemAtPosition(position);

                Toast.makeText(getContext(), "clikna na prova " + prova, Toast.LENGTH_SHORT).show();

                ProvasActivity provasactivity = (ProvasActivity) getActivity();
                provasactivity.selecionaProva(prova);
            }
        });

        return view;
    }
}
