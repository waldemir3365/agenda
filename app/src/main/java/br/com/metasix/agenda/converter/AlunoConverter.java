package br.com.metasix.agenda.converter;

import org.json.JSONException;
import org.json.JSONStringer;

import java.util.List;

import br.com.metasix.agenda.modelo.Aluno;

public class AlunoConverter {


    public String converteParaJSON(List<Aluno> alunos) throws JSONException {

        JSONStringer js = new JSONStringer();

        js.object().key("list").array().object().key("aluno").array();

        for (Aluno aluno : alunos) {

            js.object();
            js.key("nome").value(aluno.getNome());
            js.key("nota").value(aluno.getNota());
            js.endObject();
        }
        js.endArray().endObject().endArray().endObject();
        return js.toString();
    }

    public String converteParaJSONCompleto(Aluno aluno) throws JSONException {

        JSONStringer js = new JSONStringer();
        js.object()
                .key("nome").value(aluno.getNome())
                .key("endereco").value(aluno.getEndereco())
                .key("site").value(aluno.getSite())
                .key("telefone").value(aluno.getTelefone())
                .key("nota").value(aluno.getNota()).endObject();


        return js.toString();

    }
}
