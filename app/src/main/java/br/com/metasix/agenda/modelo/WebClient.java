package br.com.metasix.agenda.modelo;

import java.io.IOException;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

public class WebClient {

    public String post(String json) throws IOException {

        String endereco = "https://www.caelum.com.br/mobile";

        return realizaConexao(json, endereco);

    }

    private String realizaConexao(String json, String endereco) throws IOException {


        URL url = new URL(endereco);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        connection.setRequestProperty("Content-type", "application/json");
        connection.setRequestProperty("Accept", "application/json");

        connection.setDoOutput(true);

        PrintStream output =  new PrintStream(connection.getOutputStream());
        output.println(json);
        connection.connect();

        Scanner scanner =  new Scanner(connection.getInputStream());

        String resposta = scanner.next();

        return resposta;
    }



    public void insere(String json) throws IOException {

        String endereco = "http://10.7.30.121:8080/api/aluno";
        realizaConexao(json, endereco);

    }
}
