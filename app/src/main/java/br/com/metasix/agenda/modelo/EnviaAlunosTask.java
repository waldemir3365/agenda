package br.com.metasix.agenda.modelo;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import org.json.JSONException;

import java.io.IOException;
import java.util.List;

import br.com.metasix.agenda.converter.AlunoConverter;
import br.com.metasix.dao.AlunoDAO;

public class EnviaAlunosTask extends AsyncTask<Void, Void, String> {


    private Context context;
    private ProgressDialog dialog;


    public EnviaAlunosTask(Context context) {
        this.context = context;
    }

    @Override
    protected String doInBackground(Void... objects) {


        String json ="";

        AlunoDAO dao = new AlunoDAO(context);
        List<Aluno> alunos = dao.buscaAlunos();
        dao.close();


        AlunoConverter conversor = new AlunoConverter();
        try {

            json = conversor.converteParaJSON(alunos);
        } catch (JSONException e) {

            e.printStackTrace();
        }


        WebClient client = new WebClient();
        String resposta = "";

        try {

            resposta  = client.post(json);

        } catch (IOException e) {

            e.printStackTrace();
        }

        return resposta;
    }


    @Override
    protected void onPreExecute() {

        dialog = ProgressDialog.show(context,
                "aguarde", "Enviando Alunos", true, true);


    }

    @Override
    protected void onPostExecute(String o) {
        dialog.dismiss();
        Toast.makeText(context, o , Toast.LENGTH_LONG).show();

        super.onPostExecute(o);
    }
}
