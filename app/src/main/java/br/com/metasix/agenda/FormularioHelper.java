package br.com.metasix.agenda;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;

import br.com.metasix.agenda.modelo.Aluno;
import br.com.metasix.ui.activity.FormularioActivity;

public class FormularioHelper  {

    private final EditText campoNome;
    private final EditText campoEndereco;
    private final EditText campoTelefone;
    private final EditText campoSite;
    private final RatingBar campoNota;
    private final ImageView campofoto;

    private Aluno aluno;

    public FormularioHelper(FormularioActivity activity){


        campoNome = (EditText) activity.findViewById(R.id.formulario_nome);
        campoEndereco = (EditText)activity.findViewById(R.id.formulario_endereco);
        campoTelefone = (EditText)activity.findViewById(R.id.formulario_telefone);
        campoSite = (EditText)activity.findViewById(R.id.formulario_site);
        campoNota = (RatingBar)activity.findViewById(R.id.formulario_nota);
        campofoto = (ImageView)activity.findViewById(R.id.formulario_foto);
        aluno = new Aluno();
    }


    public Aluno pegaAluno(){


        aluno.setNome(campoNome.getText().toString());
        aluno.setEndereco(campoEndereco.getText().toString());
        aluno.setTelefone(campoTelefone.getText().toString());
        aluno.setSite(campoSite.getText().toString());
        aluno.setNota(Double.valueOf(campoNota.getProgress()));
        aluno.setCaminhofoto((String) campofoto.getTag());

        return aluno;
    }


    public void preencheForm(Aluno a) {

        campoNome.setText(a.getNome());
        campoEndereco.setText(a.getEndereco());
        campoTelefone.setText(a.getTelefone());
        campoSite.setText(a.getSite());
        campoNota.setProgress(a.getNota().intValue());
        carregaImage(a.getCaminhofoto());
        this.aluno = a;

    }

    public void carregaImage(String caminhoFoto) {

        if(caminhoFoto != null) {

            Bitmap bitmap = BitmapFactory.decodeFile(caminhoFoto);
            Bitmap bitmapReduzido = Bitmap.createScaledBitmap(bitmap, 300, 300, true);

            campofoto.setImageBitmap(bitmapReduzido);
            campofoto.setScaleType(ImageView.ScaleType.FIT_XY);
            campofoto.setTag(caminhoFoto);
        }


    }
}
