package br.com.metasix.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import br.com.metasix.agenda.modelo.Aluno;

public class AlunoDAO extends SQLiteOpenHelper {


    public AlunoDAO(Context context) {

        super(context, "Agenda", null, 2);
    }



    @Override
    public void onCreate(SQLiteDatabase db) {

        String sql = "CREATE TABLE Alunos (" +
                "id INTEGER PRIMARY KEY, " +
                "nome TEXT NOT NULL, " +
                "endereco TEXT, " +
                "telefone TEXT, " +
                "site TEXT, " +
                "nota REAL," +
                "caminhofoto TEXT);";

        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        String sql = "";
        switch(oldVersion){

            case 1:

                sql = "ALTER TABLE Alunos ADD COLUMN caminhofoto TEXT ";
                db.execSQL(sql);


        }

    }

    public void insere(Aluno aluno) {

        SQLiteDatabase db = getWritableDatabase();

        ContentValues dados = pegaDadosDoAluno(aluno);


        long id = db.insert("Alunos", null, dados);
        aluno.setId(id);

    }

    @NonNull
    private ContentValues pegaDadosDoAluno(Aluno aluno) {


        ContentValues dados  = new ContentValues();

        dados.put("nome",aluno.getNome());
        dados.put("endereco",aluno.getEndereco());
        dados.put("telefone",aluno.getTelefone());
        dados.put("site",aluno.getSite());
        dados.put("nota",aluno.getNota());
        dados.put("caminhofoto",aluno.getCaminhofoto());

        return dados;
    }

    public List<Aluno> buscaAlunos() {

        String sql = "SELECT * FROM Alunos;";

        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.rawQuery(sql, null);

        List<Aluno> alunos = new ArrayList<Aluno>();

        while(c.moveToNext()){

            Aluno a = new Aluno();

            a.setId(c.getLong(c.getColumnIndex("id")));
            a.setNome(c.getString(c.getColumnIndex("nome")));
            a.setEndereco(c.getString(c.getColumnIndex("endereco")));
            a.setTelefone(c.getString(c.getColumnIndex("telefone")));
            a.setSite(c.getString(c.getColumnIndex("site")));
            a.setNota(c.getDouble(c.getColumnIndex("nota")));
            a.setCaminhofoto(c.getString(c.getColumnIndex("caminhofoto")));

            alunos.add(a);

        }
        c.close();

        return alunos;

    }

    public void deletar(Aluno aluno) {

        SQLiteDatabase db = getWritableDatabase();

        String[] params = {aluno.getId().toString()};

        db.delete("Alunos","id = ?",params);
    }

    public void altera(Aluno aluno) {


        SQLiteDatabase db = getWritableDatabase();

        ContentValues dados = pegaDadosDoAluno(aluno);


        String[] params = {aluno.getId().toString()};

        db.update("Alunos", dados, "id = ?", params);

    }

    public boolean ehAluno(String telefone){

        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM Alunos WHERE telefone = ?", new String[]{telefone});
        int result = c.getCount();
        c.close();

        return result > 0;

    }
}
