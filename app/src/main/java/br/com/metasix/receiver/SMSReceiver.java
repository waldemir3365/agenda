package br.com.metasix.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.SmsMessage;
import android.widget.Toast;

import br.com.metasix.dao.AlunoDAO;

public class SMSReceiver extends BroadcastReceiver {


    @Override
    public void onReceive(Context context, Intent intent) {


        Object[] pdus  = (Object[]) intent.getSerializableExtra("pdus");

        byte[] pdu = (byte[]) pdus[0];

        String formato = (String) intent.getSerializableExtra("format");

        SmsMessage sms = SmsMessage.createFromPdu(pdu, formato);

        String telefone = sms.getDisplayOriginatingAddress();

        AlunoDAO dao = new AlunoDAO(context);

        if(dao.ehAluno(telefone)){

            Toast.makeText(context, "sou uma Mensagem do seu Aluno", Toast.LENGTH_LONG).show();

            //Toca um som especifico quando chega a mensagem
            //MediaPlayer m  = MediaPlayer.create(context, R.raw.msg);
            //m.start();
        }
        dao.close();

    }



}
