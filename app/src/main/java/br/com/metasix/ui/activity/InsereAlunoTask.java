package br.com.metasix.ui.activity;

import android.os.AsyncTask;

import org.json.JSONException;

import java.io.IOException;

import br.com.metasix.agenda.converter.AlunoConverter;
import br.com.metasix.agenda.modelo.Aluno;
import br.com.metasix.agenda.modelo.WebClient;

class InsereAlunoTask  extends AsyncTask {


    private Aluno aluno;

    public InsereAlunoTask(Aluno aluno) {

        this.aluno = aluno;
    }


    @Override
    protected Object doInBackground(Object[] objects) {

        String json = null;
        try {


            json = new AlunoConverter().converteParaJSONCompleto(aluno);

        } catch (JSONException e) {

            e.printStackTrace();
        }
        try {

            new WebClient().insere(json);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
