package br.com.metasix.ui.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import br.com.metasix.agenda.R;

public class ProvasTabletActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_provas_tablet);
    }
}
