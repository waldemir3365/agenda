package br.com.metasix.ui.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.File;

import br.com.metasix.agenda.FormularioHelper;
import br.com.metasix.agenda.R;
import br.com.metasix.agenda.modelo.Aluno;
import br.com.metasix.dao.AlunoDAO;
import br.com.metasix.retrofit.RetrofitInicializador;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FormularioActivity extends AppCompatActivity {


    public static final int CODIGO_CAMERA = 567;
    private FormularioHelper helper;
    private String caminhoFoto;
    private Uri fileUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario);


        helper = new FormularioHelper(this);


        Intent i = getIntent();
        Aluno a = (Aluno) i.getSerializableExtra("aluno");

        if(a != null){

            helper.preencheForm(a);

        }

        Button botaoFoto = (Button)findViewById(R.id.formulario_botao_foto);

        botaoFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intentCamera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);


                if(ActivityCompat.checkSelfPermission(FormularioActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED){

                    ActivityCompat.requestPermissions(FormularioActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            CODIGO_CAMERA);

                }

                caminhoFoto = getExternalFilesDir(null) + "/" + System.currentTimeMillis() + ".jpg";
                File arquivofoto = new  File(caminhoFoto);
                intentCamera.putExtra(MediaStore.EXTRA_OUTPUT, FileProvider.getUriForFile(FormularioActivity.this,"br.com.metasix.fileprovider", arquivofoto));
                startActivityForResult(intentCamera, CODIGO_CAMERA);

                    /*
                    ContentValues values  = new ContentValues(1);
                    values.put(MediaStore.Images.Media.MIME_TYPE, "/" + System.currentTimeMillis() + ".jpg");

                    fileUri = getApplication().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
                    intentCamera.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
                    intentCamera.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

                    startActivityForResult(intentCamera, CODIGO_CAMERA);*/


            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        //abrir a foto que nos tiramos
        if(resultCode == Activity.RESULT_OK) {
            if (requestCode == CODIGO_CAMERA) {

               helper.carregaImage(caminhoFoto);

            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

         MenuInflater inflater  =  getMenuInflater();

         inflater.inflate(R.menu.menu_formulario,menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){


            case R.id.menu_formulario_ok:


                Aluno aluno = helper.pegaAluno();

                AlunoDAO dao = new AlunoDAO(this);

                if(aluno.getId()!= null){

                    dao.altera(aluno);

                }else {


                    dao.insere(aluno);

                }

                dao.close();


               // new InsereAlunoTask(aluno).execute();

                Call call = new RetrofitInicializador().getAlunoService().insere(aluno);

                call.enqueue(new Callback() {

                    @Override
                    public void onResponse(Call call, Response response) {

                        Log.i("onResponse", "Sucessiful ");
                    }

                    @Override
                    public void onFailure(Call call, Throwable t) {

                        Log.i("onfailure", " Erro falhou");
                    }
                });

                Toast.makeText(FormularioActivity.this,"Cliente salvo com Sucesso",Toast.LENGTH_LONG).show();
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
