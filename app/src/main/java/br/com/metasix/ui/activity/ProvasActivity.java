package br.com.metasix.ui.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import br.com.metasix.DetalhesProvaFragment;
import br.com.metasix.agenda.R;
import br.com.metasix.agenda.modelo.Prova;
import br.com.metasix.fragment.ListaProvasFragment;

public class ProvasActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_provas);


        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction tx = fragmentManager.beginTransaction();

        tx.replace(R.id.frame_principal, new ListaProvasFragment());

        if(getResources().getBoolean(R.bool.modoPaisagem)){

            tx.replace(R.id.frame_secundario, new DetalhesProvaFragment());

        }

        tx.commit();


    }

    public void selecionaProva(Prova prova) {

        FragmentManager manager = getSupportFragmentManager();
       if(!getResources().getBoolean(R.bool.modoPaisagem)) {

           FragmentTransaction tx = manager.beginTransaction();

           DetalhesProvaFragment detalhesProvaFragment = new DetalhesProvaFragment();

           Bundle parametros = new Bundle();
           parametros.putSerializable("prova", prova);

           detalhesProvaFragment.setArguments(parametros);

           tx.replace(R.id.frame_principal,detalhesProvaFragment );

           tx.addToBackStack(null);

           tx.commit();
       }else{

          DetalhesProvaFragment detalhesFragment =
                  (DetalhesProvaFragment) manager.findFragmentById(R.id.frame_secundario);
          detalhesFragment.populaCamposCom(prova);

       }
    }
}
