package br.com.metasix.ui.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import java.util.List;

import br.com.metasix.adapter.AlunosAdapter;
import br.com.metasix.agenda.R;
import br.com.metasix.agenda.modelo.Aluno;
import br.com.metasix.agenda.modelo.EnviaAlunosTask;
import br.com.metasix.dao.AlunoDAO;

public class ListaAlunosActivity extends AppCompatActivity {

    private ListView listAlunos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_alunos);


        listAlunos = (ListView)findViewById(R.id.lista);

        //realizando o click no item da lista de alunos
       listAlunos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
           @Override
           public void onItemClick(AdapterView<?> lista, View item, int position, long id) {

               Aluno aluno = (Aluno) listAlunos.getItemAtPosition(position);


               Intent i  = new Intent(ListaAlunosActivity.this, FormularioActivity.class);

               i.putExtra("aluno", aluno);

               startActivity(i);

           }
       });



        Button novoAluno = findViewById(R.id.btn_novo_aluno);
        novoAluno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i  = new Intent(ListaAlunosActivity.this, FormularioActivity.class);
                startActivity(i);

            }
        });

        //adicionando menu de contexto
        registerForContextMenu(listAlunos);


    }

    private void carregaLista() {

        AlunoDAO dao = new AlunoDAO(this);

        List<Aluno> alunos =  dao.buscaAlunos();
        dao.close();

        AlunosAdapter adapter = new AlunosAdapter(this, alunos);

        listAlunos.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        carregaLista();
    }

    //metodo para menu de contexto
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, final ContextMenu.ContextMenuInfo menuInfo) {


        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        final Aluno aluno  = (Aluno)listAlunos.getItemAtPosition(info.position);


        MenuItem itemLigar = menu.add("Ligar para Contato");
        itemLigar.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                // fazendo verificação de permissao com usuario

                if(ActivityCompat.checkSelfPermission(ListaAlunosActivity.this, Manifest.permission.CALL_PHONE)
                        != PackageManager.PERMISSION_GRANTED){

                    ActivityCompat.requestPermissions(ListaAlunosActivity.this, new String[]{Manifest.permission.CALL_PHONE},
                            123);


                }else{

                    Intent intentLigar = new Intent(Intent.ACTION_CALL);
                    intentLigar.setData(Uri.parse("tel:" + aluno.getTelefone()));
                    startActivity(intentLigar);
                }



                return false;
            }
        });


        MenuItem itemSMS = menu.add("Enviar SMS");
        Intent intentSMS = new Intent(Intent.ACTION_VIEW);
        intentSMS.setData(Uri.parse("sms:" + aluno.getTelefone()));
        itemSMS.setIntent(intentSMS);



        MenuItem itemMapa = menu.add("Visualizar no mapa");
        Intent intentMapa = new Intent(Intent.ACTION_VIEW);
        intentMapa.setData(Uri.parse("geo:0,0?q=" + aluno.getEndereco()));
        itemMapa.setIntent(intentMapa);



        MenuItem itemSite = menu.add("Visitar site");
        Intent intentSite = new Intent(Intent.ACTION_VIEW);

        String site = aluno.getSite();

        if(!site.startsWith("http://")){


            site = "http://" + site;

        }

        intentSite.setData(Uri.parse(site));
        itemSite.setIntent(intentSite);


        MenuItem deletar  = menu.add("Deletar");
        //fazendo o click do menu de contexto
        deletar.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                AlunoDAO dao = new AlunoDAO(ListaAlunosActivity.this);
                dao.deletar(aluno);
                dao.close();

                carregaLista();

                return false;
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_lista_alunos, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        String json = "";

        switch (item.getItemId()){

            case R.id.menu_enviar_notas:

                new EnviaAlunosTask(this).execute();
                break;

            case R.id.menu_baixar_provas:

                Intent i  = new Intent(this, ProvasActivity.class);
                startActivity(i);
                break;

            case R.id.menu_mapa:

                Intent navegamapa  = new Intent(this, MapaActivity.class);
                startActivity(navegamapa);
                break;


        }
        return super.onOptionsItemSelected(item);
    }
}
